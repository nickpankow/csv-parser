//
//  CSVReader.swift
//  CSVSerialization
//
//  Created by Nick Pankow on 3/26/17.
//  Copyright © 2017 Nick Pankow. All rights reserved.
//

import Foundation

public class CSVReader {

    /// Configuration options for the parser
    public struct Options {
        // MARK:
        // MARK: Vars
        
        /// Defines the end of a line.
        ///
        /// Default: '\n'
        public var endOfLineChar : Character = Character("\n")
        
        /// Defines the end of a field
        ///
        /// Default: ','
        public var delimeter : Character = Character(",")
        
        /// Defines quote characters that allow for the escaping of
        /// special characters.
        ///
        /// Default: '"'
        public var quoteChar     : Character = Character("\"")
        
        /// Default Init
        public init(
            endOfLineChar eol : Character = Character("\n"),
            delimeter delim : Character = Character(","),
            quoteChar quote: Character = Character("\"")
        ) {
            endOfLineChar = eol
            delimeter = delim
            quoteChar = quote
        }
        
    }
    
    public class Iterator {
        // MARK:
        // MARK: Vars
        
        /// Configuration Options
        public let options : CSVReader.Options
        
        /// Data to parse
        public let csvData : String
        
        /// Current parsing position in self.csvData
        public internal(set) var parseIndex : String.Index? = nil
        
        // MARK:
        // MARK: Constructors
        
        /**
         Initialize with the data as a String
         
         - Parameter data: The string to parse
         */
        public init(data: String, options opts: CSVReader.Options = CSVReader.Options()) {
            csvData = data
            options = opts
        }
        
        // MARK:
        // MARK: Parsing
        
        /**
         Iterates through the data and returns an array containing
         the new row in the document.
         
         while let row = csv.row() {
         // code
         }
         
         - Returns: An array containing a String per column.  nil if EoF
         */
        public func row() -> [String]? {
            if parseIndex == nil {
                parseIndex = csvData.startIndex
            }
            
            guard parseIndex != csvData.endIndex else {
                return nil
            }
            
            var fieldData : [Character] = []
            var fields : [String] = []

            var escapesFound : Int = 0
            
            repeat {
                // Get character
                let char : Character = csvData[parseIndex!]
                parseIndex = csvData.index(after: parseIndex!)
                
                switch char {
                case options.quoteChar:
                    // Add to quote count
                    escapesFound += 1
                    if escapesFound == 3 {
                        // Escaped escape chacter
                        // example: """asfa" => "asfa
                        fieldData.append(char)
                        escapesFound -= 2
                        break
                    }
                case options.endOfLineChar:
                    // Check for escaped character.
                    if escapesFound != 0 || escapesFound == 2 {
                        fieldData.append(char)
                        break
                    } else {
                        // We're in the escaped range, save the char
                        fields.append(String(fieldData))
                        fieldData = []
                        return fields
                    }
                case options.delimeter:
                    // Save the char if it's not escaped
                    if escapesFound != 0 && escapesFound != 2 {
                        fieldData.append(char)
                        break
                    } else {
                        // Otherwise, we're at the end of the field. Save it
                        fields.append(String(fieldData))
                        fieldData = []
                        
                        escapesFound -= 2
                        if escapesFound < 0 {
                            escapesFound = 0
                        }
                        break
                    }
                default:
                    // No matches, append char
                    fieldData.append(char)
                }
                
                // Are we at the end?
            } while parseIndex != csvData.endIndex
            
            
            return nil
        }
    }

    /// Init with String data
    public static func withString(
        data: String,
        options: CSVReader.Options = CSVReader.Options()
    ) -> CSVReader.Iterator {
        return CSVReader.Iterator(data: data, options: options)
    }
    
    /// Init with Data encoded String
    public static func withData(
        data: Data,
        encoding: String.Encoding,
        options: CSVReader.Options = CSVReader.Options()
    ) -> CSVReader.Iterator? {
        if let data = String(data: data, encoding: encoding) {
            return CSVReader.Iterator(data: data, options: options)
        } else {
            return nil
        }
    }
    
    /** 
        Parse entire document at once
     
        - Parameter withString: String data to parse
 
     */
    public static func parse(
        string data : String,
        headers     : Bool = false,
        options     : CSVReader.Options = CSVReader.Options()
    ) -> CSV? {
        
        var csvData : [[String]] = []
        let csv = CSVReader.withString(data: data)
        
        for row in csv {
            csvData.append(row)
        }
        
        return CSV(with: csvData, headers: headers)
    }
}

// MARK: Sequence
// MARK:

extension CSVReader.Iterator : IteratorProtocol, Sequence {
    
    /// IteratorProtocol requirement.  Wrapper for next().
    public func next() -> [String]? {
        return self.row()
    }
}
