//
//  CSV.swift
//  CSVSerialization
//
//  Created by Nick Pankow on 7/16/17.
//  Copyright © 2017 Nick Pankow. All rights reserved.
//

import Foundation

/// Container for parsed csv data.
public class CSV {
    
    /// Get header row
    public let hasHeaders : Bool
    public var headers : [String]? {
        get {
            return hasHeaders ? self.rows[0] : nil
        }
    }
    
    /// Rows of CSV
    public var rows : [[String]]
    
    /** 
     Initialize with data and a flag to indicate if the data 
     contains headers.
     
     - Parameter rows: csv data
     - Parameter havingHeaders: Is the first row of the data headers?
     */
    public init(with rows : [[String]], headers hasHeaders: Bool = false) {
        self.rows = rows
        self.hasHeaders = hasHeaders
    }
    
    /**
     Create a CSV object from a csv file at a given path
     
     Throws CSVReadingError on failure
     
     - Parameter path: File path
     - Parameter encoding: File content string encoding
     - Parameter options: CSV parsing options
     
     - Returns: CSV
     */
    public static func load(
        at path: URL,
        headers hasHeaders: Bool,
        with encoding: String.Encoding = .utf8,
        using options: CSVReader.Options = CSVReader.Options()
    ) throws -> CSV {
        guard let csvContents = FileManager.default.contents(atPath: path.path) else {
            throw CSVReadingError.fileNotFound
        }
        
        guard let csvString = String(data: csvContents, encoding: encoding) else {
            throw CSVReadingError.encodingFailure
        }
        
        if let csv = CSVReader.parse(string: csvString, headers: hasHeaders, options: options) {
            return csv
        } else {
            throw CSVReadingError.parsingFailure
        }
    }
}
