//
//  CSVErrors.swift
//  CSVSerialization
//
//  Created by Nick Pankow on 7/16/17.
//  Copyright © 2017 Nick Pankow. All rights reserved.
//

import Foundation

public enum CSVReadingError : Error {
    case fileNotFound
    case encodingFailure
    case parsingFailure
}
