# CSVSerialization

Read and write delimiter separated documents.

## Decoding

```swift
let data = "1,2,3\n4,5,6\n"
let csv = CSVReader.withString(data: data)

for row in csv {
    // Array containing row data
    // ["1", "2", "3"]
    // ["4", "5", "6"]
}
```

### Custom decoding options
Configure the following decoding options using CSVReader.Options:  
* End of line character  
* Delimeter character  
* Quote character  


###### Options
```swift
CSVReader.Options(
	endOfLineChar: Character("\r\n"), 
	delimeter: Character(";"), 
	quoteChar: Character("\"")
)
```


###### Initialize Decoder with Options
```swift
let csv = CSVReader.withString(
	data: data,
	options: CSVReader.Options(endOfLineChar: Character("\r\n"))
)
```