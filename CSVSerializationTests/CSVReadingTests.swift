//
//  CSVReadingTests.swift
//  CSVSerialization
//
//  Created by Nick Pankow on 3/4/17.
//  Copyright © 2017 Nick Pankow. All rights reserved.
//

import XCTest

@testable import CSVSerialization

class CSVReadingTests: XCTestCase {
    
    var testBundle : Bundle? = nil
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        testBundle = Bundle(for: type(of: self))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    private func loadContentsOfFileOrAssert(name: String, ext: String) -> String {
        guard let dataPath = testBundle?.path(forResource: name, ofType: ext) else {
            XCTAssert(false, "Failed to find data file: \(name).\(ext)")
            return ""
        }
        
        guard let data = try? String(contentsOfFile: dataPath) else {
            XCTAssert(false, "Failed to load test data from file: \(name).\(ext)")
            return ""
        }
        
        return data
    }
    
    func testBasicCSV() {
        
        let data = loadContentsOfFileOrAssert(name: "basic", ext: "csv")
        
        let csv = CSVReader.withString(data: data)
        
        let expectedRows = [
            ["1","2","3","4","5","6","7","8","9","0"],
            ["a","b","c","d","e","f","g","h","i","j"],
            [".","/","'",";","[","]","\\","-","=","`"],
            ["<",">","?",":","\"","{","}","|","_","+"],
            ["\n","2","3","4","5","6","7","8","9"],
        ]

        var i = 0
        while let row = csv.row() {
            XCTAssertEqual(row, expectedRows[i], "Row #\(i) Data did not match expected")
            i += 1
        }
        
        XCTAssert(i == expectedRows.count, "Number of rows found did not match expected in file \(i), \(expectedRows.count)")
        
        let path = testBundle!.url(forResource: "basic", withExtension: "csv")!
        
        var csv2 : CSV
        do {
            csv2 = try CSV.load(at: path, headers: false)
        } catch let e {
            XCTAssert(false, "\(e.localizedDescription)")
            return
        }
        
        i = 0
        for row in csv2.rows {
            XCTAssertEqual(row, expectedRows[i], "Row #\(i) Data did not match expected")
            i += 1
        }
    }
    
    func testCSVWithEmbeddedDelimeter() {
        
        let data = loadContentsOfFileOrAssert(name: "embedded_delimeter", ext: "csv")
        let csv = CSVReader.withString(data: data)
        
        let expectedRows = [
            ["Year","Make","Model","Description","Price"],
            ["1997","Ford","E350","ac, abs, moon", "3000.00"],
            ["1999","Chevy","Venture \"Extended Edition\"","","4900.00"],
            ["1999","Chevy","Venture \"Extended Edition, Very Large\"","","5000.00"],
            ["1996","Jeep","Grand Cherokee","MUST SELL!\nair, moon roof, loaded","4799.00"],
        ]
        
        var i = 0
        while let row = csv.row() {
            XCTAssertEqual(row, expectedRows[i], "Row #\(i) Data did not match expected")
            i += 1
        }

        
        XCTAssert(i == expectedRows.count, "Number of rows found did not match expected in file")
        
    }
    
    func testCSVWithCusomDelimeter() {
        let data = loadContentsOfFileOrAssert(name: "custom_delimeter", ext: "csv")
        let csv = CSVReader.withString(
            data: data,
            options: CSVReader.Options(delimeter: Character(";"))
        )
        
        let expectedRows = [
            ["5","6","7","8","9","0"],
            ["1","2","3","4","5","6"],
            ["abc","def","ghi","jkl","mnop","qrstuvwxyz"]
        ]
        
        var i = 0
        while let row = csv.row() {
            XCTAssertEqual(row, expectedRows[i], "Row #\(i) did not match expected data found in file")
            i += 1
        }
        
        XCTAssertEqual(i, expectedRows.count, "Number of rows did not match expected in file")
    }
    
    func testLargePublicCSVFile() {
        
        let expectedFields = 122
        let expectedNumRows = 7704
        
        var data : String = ""
        
        data = loadContentsOfFileOrAssert(name: "Most-Recent-Cohorts-Scorecard-Elements", ext: "csv")
        
        let csv = CSVReader.withString(
            data: data,
            options: CSVReader.Options(endOfLineChar: Character("\r\n"))
        )
        var i = 0
        
        while let row = csv.row() {
            i += 1
            if row.count != expectedFields {
                XCTAssert(false, "Number of fields did not match expected.  Found: \(row.count)")
                return
            }
        }
        
        XCTAssertEqual(i, expectedNumRows, "Number of rows in file did not match expected.")
    }
}
