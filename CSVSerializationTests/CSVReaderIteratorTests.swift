//
//  CSVReaderIteratorTests.swift
//  CSVSerialization
//
//  Created by Nick Pankow on 3/21/17.
//  Copyright © 2017 Nick Pankow. All rights reserved.
//

import XCTest

@testable import CSVSerialization

class CSVReaderIteratorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIteratorForLoop() {
        
        let data = "1,2,3\na,b,c\n"
        
        let csv = CSVReader.withString(data: data)
        
        let expectedRows = [
            ["1","2","3"],
            ["a","b","c"],
        ]
        
        var i = 0
        for row in csv {
            XCTAssertEqual(row, expectedRows[i], "Row #\(i) Data did not match expected")
            i += 1
        }
        
        XCTAssert(i == expectedRows.count, "Number of rows found did not match expected in file \(i), \(expectedRows.count)")
    }
    
}
